package samples;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JsonHelper {

  @SuppressWarnings("unchecked")
  private static JSONObject createJson(String type) {
    JSONObject json = new JSONObject();
    json.put("type", type);
    return json;
  }

  @SuppressWarnings("unchecked")
  public static String createResponse(String type, JSONArray array) {
    final JSONObject json = JsonHelper.createJson(type);
    json.put("calls", array);
    return json.toJSONString();
  }

  @SuppressWarnings("unchecked")
  public static String createResponse(String type, int status, int time) {
    final JSONObject json = JsonHelper.createJson(type);
    json.put("status", String.valueOf(status));
    json.put("time", time);
    return json.toJSONString();
  }

}
