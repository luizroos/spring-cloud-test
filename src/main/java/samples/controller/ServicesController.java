package samples.controller;

import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.math.RandomUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import rx.Observable;
import samples.JsonHelper;
import samples.client.LocalhostService;

import com.netflix.servo.annotations.DataSourceLevel;
import com.netflix.servo.annotations.DataSourceType;
import com.netflix.servo.annotations.Monitor;
import com.netflix.servo.annotations.MonitorTags;
import com.netflix.servo.monitor.Monitors;
import com.netflix.servo.tag.BasicTagList;
import com.netflix.servo.tag.TagList;

@RestController
public class ServicesController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ServicesController.class);

  @Autowired
  private LocalhostService localhostService;


  @Monitor(name = "luiz_requestCounter", type = DataSourceType.COUNTER,
      description = "Total number of requests", level = DataSourceLevel.INFO)
  private final AtomicInteger requestCounter = new AtomicInteger(0);

  @Monitor(name = "luiz_aGauge", type = DataSourceType.GAUGE, description = "A random gauge",
      level = DataSourceLevel.CRITICAL)
  private final AtomicInteger aGauge = new AtomicInteger(0);

  @PostConstruct
  public void init() {
    Monitors.registerObject("ServicesController", this);
  }

  @SuppressWarnings("unchecked")
  @ResponseBody
  @GetMapping(value = "/services/x", produces = MediaType.APPLICATION_JSON_VALUE)
  public String serviceX() throws Exception {
    LOGGER.info("serviceX");
    

    requestCounter.incrementAndGet(); // increment counter
    aGauge.set(RandomUtils.nextInt()); // set some random value
    
    final Observable<String> r1 = localhostService.callServiceY();
    final Observable<String> r2 = localhostService.callTransactions(random(510));
    final Observable<String> r3 = localhostService.callUsers(HttpStatus.OK.value(), random(350));
    return JsonHelper.createResponse("serviceX", toJsonArray(r1, r2, r3));
  }

  @SuppressWarnings("unchecked")
  @ResponseBody
  @GetMapping(value = "/services/y", produces = MediaType.APPLICATION_JSON_VALUE)
  public String serviceXyz() throws Exception {
    LOGGER.info("serviceY");
    final Observable<String> r1 = localhostService.callTransactions(random(200));
    final Observable<String> r2 =
        localhostService.callUsers(HttpStatus.BAD_GATEWAY.value(), random(100));
    final Observable<String> r3 = localhostService.callTransactions(random(510));
    final Observable<String> r4 = localhostService.callUsers(HttpStatus.OK.value(), random(350));
    return JsonHelper.createResponse("serviceY", toJsonArray(r1, r2, r3, r4));
  }

  @ResponseBody
  @GetMapping(value = "/transactions/xyz/{time}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> transactionsService(@PathVariable("time") int time)
      throws Exception {
    LOGGER.info("transactions,time={}", time);
    Thread.sleep(time);
    return new ResponseEntity<>(JsonHelper.createResponse("transaction", HttpStatus.OK.value(),
        time), HttpStatus.OK);
  }

  @ResponseBody
  @GetMapping(value = "/users/xyz/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> usersService(@PathVariable("status") int statusCode,
      @RequestParam("time") int time) throws Exception {
    LOGGER.info("users,status={},time={}", statusCode, time);
    Thread.sleep(time);
    return new ResponseEntity<>(JsonHelper.createResponse("user", statusCode, time),
        HttpStatus.valueOf(statusCode));
  }

  @SuppressWarnings("unchecked")
  private static JSONArray toJsonArray(Observable<String>... sequences) {
    final JSONArray array = new JSONArray();
    Observable.merge(sequences).map(r -> parseJson(r))//
        .toBlocking()//
        .subscribe(r -> array.add(r));
    return array;
  }

  private static int random(int max) {
    return (int) (Math.random() * max);
  }

  private static JSONObject parseJson(String json) {
    final JSONParser parser = new JSONParser();
    try {
      return (JSONObject) parser.parse(json);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }


}
