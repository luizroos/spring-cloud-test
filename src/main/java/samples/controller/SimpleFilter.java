package samples.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class SimpleFilter extends ZuulFilter {

  @Override
  public boolean shouldFilter() {
    return true;
  }

  @Override
  public Object run() {
    RequestContext ctx = RequestContext.getCurrentContext();
    HttpServletRequest request = ctx.getRequest();

    ctx.setResponseStatusCode(HttpStatus.OK.value());

    System.out.println(String.format("%s request to %s", request.getMethod(), request
        .getRequestURL().toString()));

    return null;
  }

  @Override
  public String filterType() {
    return "post";
  }

  @Override
  public int filterOrder() {
    return 1;
  }

}
