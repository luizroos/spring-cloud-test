package samples.client;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import rx.Observable;
import samples.JsonHelper;

@Service
public class LocalhostServiceFallback implements LocalhostService {

  private static final Logger LOGGER = LoggerFactory.getLogger(LocalhostServiceFallback.class);

  @Override
  public Observable<String> callTransactions(int time) {
    LOGGER.warn("transactions,time={}", time);
    return Observable.just(JsonHelper.createResponse("transactionFallback", 500, time));
  }

  @Override
  public Observable<String> callUsers(int status, int time) {
    LOGGER.warn("users,status={},time={}", status, time);
    return Observable.just(JsonHelper.createResponse("userFallback", status, time));
  }

  @Override
  public Observable<String> callServiceY() {
    LOGGER.warn("callServiceY");
    return Observable.just(JsonHelper.createResponse("serviceYFallback", new JSONArray()));
  }

}
