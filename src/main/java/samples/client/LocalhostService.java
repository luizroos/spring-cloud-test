package samples.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rx.Observable;
import samples.client.LocalhostService.LocalhostServiceConf;
import feign.Logger;

@Component
@FeignClient(name = "localhost", url = "http://localhost:19838",
    configuration = LocalhostServiceConf.class, fallback = LocalhostServiceFallback.class)
public interface LocalhostService {

  @RequestMapping(path = "/t/xyz/{time}", method = RequestMethod.GET)
  Observable<String> callTransactions(@PathVariable("time") int time);

  @RequestMapping(path = "/u/xyz/{status}", method = RequestMethod.GET)
  Observable<String> callUsers(@PathVariable("status") int status, @RequestParam("time") int time);

  @RequestMapping(path = "/s/y", method = RequestMethod.GET)
  Observable<String> callServiceY();

  public class LocalhostServiceConf {
    @Bean
    Logger.Level feignLoggerLevel() {
      return Logger.Level.BASIC;
    }
  }
}
